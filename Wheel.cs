
using UnityEngine;
using Oxmond.Pagehandler;


{
    public class Wheel : MonoBehaviour
    {

        /////*******************************************/////
        /////             PUBLIC VARIABLES              /////  
        /////*******************************************/////

        public GameObject selectClientLogin;
        public GameObject selectTutorial;
        public GameObject selectTryMe;
        public GameObject selectAbout;
        public GameObject textClientLogin;
        public GameObject textTutorial;
        public GameObject textTryMe;
        public GameObject textAbout;
        public GameObject wheelContainer;

        /////*******************************************/////
        /////            LOCAL VARIABLES                /////  
        /////*******************************************/////

        private bool isDragging;
        private Quaternion oldRotation;
        private float oldMouseAngle;
        private float currentMouseAngle;
        private float angleD;
        private float currentPosition, lastPosition1, lastPosition2, lastPosition3, lastPosition4, lastPosition5, lastPosition6 = 0;
        private float dragSpeedSlowDown = 0.96f;
        private float dragSpeedTreshold = 2.4f;
        private bool isRotating = false;
        private float homePosition;
        private float dragSpeed = 0;
        // private float angleThreshold = 60f;
        private float dragMaxSpeed = 20f;
        private float rotationDir = 0f;
        private float p1x;
        private float p1y;
        private float p2x;
        private float p2y;
        private float deltaX;
        private float deltaY;
        private string nextPage;
        private Pages pagehandler;
        private float mousePositionStartX;
        private float mousePositionStartY;
        private bool mouseIsActive = false;
        private string dragFunction;
        private float triggerLimit = 0.3f;
        private float selectFadeOutTime = 0.5f;
        private iTween.EaseType selectFadeOutEase = iTween.EaseType.easeOutQuint;
        private Material spriteTextClientLogin;
        private Material spriteTextTutorial;
        private Material spriteTextTryMe;
        private Material spriteTextAbout;
        private float alphaTxtHome = 0.7f;
        private float wheelContainerDefaultY;
        private float currentWheelPosition;

        /////*******************************************/////
        /////                   INIT                    /////  
        /////*******************************************/////

        private void OnEnable()
        {
            wheelContainer.transform.localPosition = new Vector2(Stage.posXCenter, Stage.posYBottom + 1.3f);
            wheelContainerDefaultY = wheelContainer.transform.localPosition.y;
            pagehandler = GameObject.Find("Pages").GetComponent<Pages>();
            iTween.FadeTo(selectClientLogin, iTween.Hash("alpha", alphaTxtHome, "time", 0));
            iTween.FadeTo(selectTutorial, iTween.Hash("alpha", 0, "time", 0));
            iTween.FadeTo(selectTryMe, iTween.Hash("alpha", 0, "time", 0));
            iTween.FadeTo(selectAbout, iTween.Hash("alpha", 0, "time", 0));
            iTween.RotateTo(this.gameObject, iTween.Hash("z", 0, "time", 0.0f));

            iTween.FadeTo(textTutorial, iTween.Hash("alpha", 0, "time", 0));
            iTween.FadeTo(textTryMe, iTween.Hash("alpha", 0, "time", 0));
            iTween.FadeTo(textAbout, iTween.Hash("alpha", 0, "time", 0));

            spriteTextClientLogin = textClientLogin.GetComponent<SpriteRenderer>().material;
            spriteTextTutorial = textTutorial.GetComponent<SpriteRenderer>().material;
            spriteTextTryMe = textTryMe.GetComponent<SpriteRenderer>().material;
            spriteTextAbout = textAbout.GetComponent<SpriteRenderer>().material;
        }

        private void Start()
        {
            oldMouseAngle = 0;
        }

        /////*******************************************/////
        /////                METHODS                    /////  
        /////*******************************************/////

        private void Update()
        {
            CheckMouseIntentions();
            UpdateDrag();
        }

        private void CheckMouseIntentions()
        {
            if (mouseIsActive == true)
            {
                if (dragFunction == "unknown")
                {
                    if (Stage.mousePosY > mousePositionStartY + triggerLimit || Stage.mousePosY < mousePositionStartY - triggerLimit)
                    {
                        dragFunction = "vertical";
                        iTween.FadeTo(selectClientLogin, iTween.Hash("alpha", 0, "time", selectFadeOutTime, "easetype", selectFadeOutEase));
                        iTween.FadeTo(selectTutorial, iTween.Hash("alpha", 0, "time", selectFadeOutTime, "easetype", selectFadeOutEase));
                        iTween.FadeTo(selectTryMe, iTween.Hash("alpha", 0, "time", selectFadeOutTime, "easetype", selectFadeOutEase));
                        iTween.FadeTo(selectAbout, iTween.Hash("alpha", 0, "time", selectFadeOutTime, "easetype", selectFadeOutEase));
                    }
                    else if (Stage.mousePosX > mousePositionStartX + triggerLimit || Stage.mousePosX < mousePositionStartX - triggerLimit)
                    {
                        dragFunction = "horizontal";
                        iTween.FadeTo(selectClientLogin, iTween.Hash("alpha", 0, "time", selectFadeOutTime, "easetype", selectFadeOutEase));
                        iTween.FadeTo(selectTutorial, iTween.Hash("alpha", 0, "time", selectFadeOutTime, "easetype", selectFadeOutEase));
                        iTween.FadeTo(selectTryMe, iTween.Hash("alpha", 0, "time", selectFadeOutTime, "easetype", selectFadeOutEase));
                        iTween.FadeTo(selectAbout, iTween.Hash("alpha", 0, "time", selectFadeOutTime, "easetype", selectFadeOutEase));
                    }
                }
                if (dragFunction == "vertical") { }
                if (dragFunction == "horizontal") { }
            }
        }

        private void UpdateDrag()
        {
            if (isDragging)
            {
                Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                p1x = mousePosition.x;
                p1y = mousePosition.y;
                p2x = this.transform.position.x;
                p2y = this.transform.position.y;
                deltaX = p1x - p2x;
                deltaY = p1y - p2y;
                angleD = Mathf.Atan2(deltaX, deltaY) * Mathf.Rad2Deg;
                currentMouseAngle = angleD;
                float newMouseAngle = oldMouseAngle - currentMouseAngle;
                transform.rotation = oldRotation * Quaternion.Euler(0, 0, oldMouseAngle - currentMouseAngle);
                CalcSpeed();
            }
            else
            {
                if (isRotating)
                {
                    transform.Rotate(new Vector3(0f, 0f, dragSpeed * 25f * rotationDir) * Time.deltaTime);
                    if (dragSpeed < dragSpeedTreshold && dragSpeed > -dragSpeedTreshold)
                    {
                        SlowDown();
                    }
                }
            }
            dragSpeed = dragSpeed * dragSpeedSlowDown;
            currentWheelPosition = transform.localEulerAngles.z;

            if (currentWheelPosition >= 315f || currentWheelPosition < 45f)
            {
                Color color = spriteTextClientLogin.color;
                if (currentWheelPosition < 45f)
                {
                    color.a = 1f - (currentWheelPosition / 45f);
                }
                if (currentWheelPosition >= 315f)
                {
                    color.a = (currentWheelPosition - 315f) / 45f;
                }
                spriteTextClientLogin.color = color;
                color.a = 0;
                spriteTextTutorial.color = color;
                spriteTextTryMe.color = color;
                spriteTextAbout.color = color;
            }

            if (currentWheelPosition >= 45f && currentWheelPosition < 135f)
            {
                homePosition = 90f;
                Color color = spriteTextTutorial.color;
                if (currentWheelPosition >= 45f && currentWheelPosition < 90f)
                {
                    color.a = (currentWheelPosition - 45f) / 45f;
                }
                if (currentWheelPosition >= 90f)
                {
                    color.a = 1f - ((currentWheelPosition - 90f) / 45f);
                }
                spriteTextTutorial.color = color;
                color.a = 0;
                spriteTextClientLogin.color = color;
                spriteTextTryMe.color = color;
                spriteTextAbout.color = color;
            }

            if (currentWheelPosition >= 135f && currentWheelPosition < 225f)
            {
                homePosition = 180f;
                Color color = spriteTextTryMe.color;
                if (currentWheelPosition >= 135f && currentWheelPosition < 180f)
                {
                    color.a = (currentWheelPosition - 135f) / 45f;
                }
                if (currentWheelPosition >= 180f)
                {
                    color.a = 1f - ((currentWheelPosition - 180f) / 45f);
                }
                spriteTextTryMe.color = color;
                color.a = 0;
                spriteTextClientLogin.color = color;
                spriteTextTutorial.color = color;
                spriteTextAbout.color = color;
            }

            if (currentWheelPosition >= 225f && currentWheelPosition < 315f)
            {
                homePosition = 270f;
                Color color = spriteTextAbout.color;
                if (currentWheelPosition >= 225f && currentWheelPosition < 270f)
                {
                    color.a = (currentWheelPosition - 225f) / 45f;
                }
                if (currentWheelPosition >= 270f)
                {
                    color.a = 1f - ((currentWheelPosition - 270f) / 45f);
                }
                spriteTextAbout.color = color;
                color.a = 0;
                spriteTextClientLogin.color = color;
                spriteTextTutorial.color = color;
                spriteTextTryMe.color = color;
            }
        }

        private void SlowDown()
        {
            if (dragFunction != "unknown")
            {
                dragSpeed = 0;
                isRotating = false;
                iTween.RotateTo(this.gameObject, iTween.Hash("z", transform.localEulerAngles.z + (10f * rotationDir), "time", 0.8f, "easetype", iTween.EaseType.easeOutQuint, "oncomplete", "RotateHome", "oncompletetarget", gameObject));
            }
        }

        private void RotateHome()
        {
            iTween.Stop(this.gameObject);
            float currentWheelPosition = transform.localEulerAngles.z;
            float selectFadeInTime = 2.5f;
            iTween.EaseType selectFadeInEase = iTween.EaseType.easeOutQuint;

            if (currentWheelPosition >= 315f || currentWheelPosition < 45f)
            {
                homePosition = 0f;
                iTween.FadeTo(selectClientLogin, iTween.Hash("alpha", alphaTxtHome, "time", selectFadeInTime, "easetype", selectFadeInEase));
            }

            if (currentWheelPosition >= 45f && currentWheelPosition < 135f)
            {
                homePosition = 90f;
                iTween.FadeTo(selectTutorial, iTween.Hash("alpha", alphaTxtHome, "time", selectFadeInTime, "easetype", selectFadeInEase));
            }

            if (currentWheelPosition >= 135f && currentWheelPosition < 225f)
            {
                homePosition = 180f;
                iTween.FadeTo(selectTryMe, iTween.Hash("alpha", alphaTxtHome, "time", selectFadeInTime, "easetype", selectFadeInEase));
            }

            if (currentWheelPosition >= 225f && currentWheelPosition < 315f)
            {
                homePosition = 270f;
                iTween.FadeTo(selectAbout, iTween.Hash("alpha", alphaTxtHome, "time", selectFadeInTime, "easetype", selectFadeInEase));
            }

            iTween.RotateTo(this.gameObject, iTween.Hash("z", homePosition, "time", 0.8f, "easetype", iTween.EaseType.easeInOutQuart));

        }

        private void CalcSpeed()
        {
            lastPosition6 = lastPosition5;
            lastPosition5 = lastPosition4;
            lastPosition4 = lastPosition3;
            lastPosition3 = lastPosition2;
            lastPosition2 = lastPosition1;
            lastPosition1 = currentPosition;
            currentPosition = transform.localEulerAngles.z;
        }

        public void OnMouseDown()
        {
            iTween.Stop(this.gameObject);
            rotationDir = 0;
            mouseIsActive = true;
            dragFunction = "unknown";
            mousePositionStartX = Stage.mousePosX;
            mousePositionStartY = Stage.mousePosY;
            float p2x = this.transform.position.x;
            float p2y = this.transform.position.y;
            float deltaX = mousePositionStartX - p2x;
            float deltaY = mousePositionStartY - p2y;
            float angleR = Mathf.Atan2(deltaX, deltaY);
            angleD = Mathf.Atan2(deltaX, deltaY) * Mathf.Rad2Deg;
            isDragging = true;
            isRotating = true;
            oldRotation = this.transform.rotation;
            oldMouseAngle = angleD;
            iTween.MoveTo(wheelContainer, iTween.Hash("y", wheelContainerDefaultY - 0.07f, "time", 0.2f, "islocal", true));
        }

        public void OnMouseUp()
        {
            isDragging = false;
            mouseIsActive = false;
            float selectFaceAnimationTime = 0.5f;

            float clickLimit = 0.1f;
            if ((mousePositionStartX - Stage.mousePosX) < clickLimit && (mousePositionStartX - Stage.mousePosX) > -clickLimit)
            {
                float mousePositionUpPosX = Stage.mousePosX;
                float mousePositionUpPosY = Stage.mousePosY;
                float deltaX = mousePositionUpPosX - p2x;
                float deltaY = mousePositionUpPosY - p2y;
                float mouseUpAngleR = Mathf.Atan2(deltaX, deltaY);
                float mouseUpAngleD = Mathf.Atan2(deltaX, deltaY) * Mathf.Rad2Deg;
                float currentWheelPosition = transform.localEulerAngles.z;
                float currentMouseWheelTouchAngle = mouseUpAngleD + currentWheelPosition;
                if (currentMouseWheelTouchAngle >= 360f) { currentMouseWheelTouchAngle -= 360f; }
                if (currentMouseWheelTouchAngle < 0f) { currentMouseWheelTouchAngle += 360f; }
                if (currentMouseWheelTouchAngle >= 315f || currentMouseWheelTouchAngle < 45f)
                {
                    nextPage = "clientlogin";
                    iTween.FadeTo(selectClientLogin, iTween.Hash("alpha", 1, "time", selectFaceAnimationTime, "easetype", selectFadeOutEase, "oncomplete", "GoToSelectedPage", "oncompletetarget", gameObject));
                    iTween.FadeTo(selectTutorial, iTween.Hash("alpha", 0, "time", selectFaceAnimationTime, "easetype", selectFadeOutEase));
                    iTween.FadeTo(selectTryMe, iTween.Hash("alpha", 0, "time", selectFaceAnimationTime, "easetype", selectFadeOutEase));
                    iTween.FadeTo(selectAbout, iTween.Hash("alpha", 0, "time", selectFaceAnimationTime, "easetype", selectFadeOutEase));
                    iTween.RotateTo(this.gameObject, iTween.Hash("z", 0, "time", 0.8f, selectFaceAnimationTime, iTween.EaseType.easeInOutQuart));
                }
                if (currentMouseWheelTouchAngle >= 45 && currentMouseWheelTouchAngle < 135f)
                {
                    nextPage = "tutorial";
                    iTween.FadeTo(selectTutorial, iTween.Hash("alpha", 1, "time", selectFaceAnimationTime, "easetype", selectFadeOutEase, "oncomplete", "GoToSelectedPage", "oncompletetarget", gameObject));
                    iTween.FadeTo(selectClientLogin, iTween.Hash("alpha", 0, "time", selectFaceAnimationTime, "easetype", selectFadeOutEase));
                    iTween.FadeTo(selectTryMe, iTween.Hash("alpha", 0, "time", selectFaceAnimationTime, "easetype", selectFadeOutEase));
                    iTween.FadeTo(selectAbout, iTween.Hash("alpha", 0, "time", selectFaceAnimationTime, "easetype", selectFadeOutEase));
                    iTween.RotateTo(this.gameObject, iTween.Hash("z", 90, "time", 0.8f, selectFaceAnimationTime, iTween.EaseType.easeInOutQuart));
                }
                if (currentMouseWheelTouchAngle >= 135 && currentMouseWheelTouchAngle < 225f)
                {
                    nextPage = "tryme";
                    iTween.FadeTo(selectTryMe, iTween.Hash("alpha", 1, "time", selectFaceAnimationTime, "easetype", selectFadeOutEase, "oncomplete", "GoToSelectedPage", "oncompletetarget", gameObject));
                    iTween.FadeTo(selectClientLogin, iTween.Hash("alpha", 0, "time", selectFaceAnimationTime, "easetype", selectFadeOutEase));
                    iTween.FadeTo(selectAbout, iTween.Hash("alpha", 0, "time", selectFaceAnimationTime, "easetype", selectFadeOutEase));
                    iTween.FadeTo(selectTutorial, iTween.Hash("alpha", 0, "time", selectFaceAnimationTime, "easetype", selectFadeOutEase));
                    iTween.RotateTo(this.gameObject, iTween.Hash("z", 180, "time", 0.8f, selectFaceAnimationTime, iTween.EaseType.easeInOutQuart));
                }
                if (currentMouseWheelTouchAngle >= 225 && currentMouseWheelTouchAngle < 315f)
                {
                    nextPage = "about";
                    iTween.FadeTo(selectAbout, iTween.Hash("alpha", 1, "time", selectFaceAnimationTime, "easetype", selectFadeOutEase, "oncomplete", "GoToSelectedPage", "oncompletetarget", gameObject));
                    iTween.FadeTo(selectClientLogin, iTween.Hash("alpha", 0, "time", selectFaceAnimationTime, "easetype", selectFadeOutEase));
                    iTween.FadeTo(selectTryMe, iTween.Hash("alpha", 0, "time", selectFaceAnimationTime, "easetype", selectFadeOutEase));
                    iTween.FadeTo(selectTutorial, iTween.Hash("alpha", 0, "time", selectFaceAnimationTime, "easetype", selectFadeOutEase));
                    iTween.RotateTo(this.gameObject, iTween.Hash("z", 270, "time", 0.8f, selectFaceAnimationTime, iTween.EaseType.easeInOutQuart));
                }
            }
            else
            {
                CalcDragSpeed();
            }
            iTween.MoveTo(wheelContainer, iTween.Hash("y", wheelContainerDefaultY, "time", 0.8f, "islocal", true, "easetype", iTween.EaseType.easeOutQuint));
        }

        private void GoToSelectedPage()
        {
            if (AppData.clientLoggedIn && nextPage == "clientlogin")
            {
                pagehandler.RightToLeft("clientcards");
            }
            pagehandler.RightToLeft(nextPage);
        }

        private void CalcDragSpeed()
        {
            if (lastPosition3 > lastPosition1)
            {
                if (lastPosition3 - lastPosition1 > 180)
                {
                    rotationDir = 1;
                }
                else
                {
                    rotationDir = -1;
                }
            }
            else if (lastPosition1 > lastPosition3)
            {
                if (lastPosition1 - lastPosition3 > 180)
                {
                    rotationDir = -1;
                }
                else
                {
                    rotationDir = 1;
                }
            }

            dragSpeed = 0f;

            if (lastPosition1 > lastPosition3)
            {
                if (lastPosition1 - lastPosition3 < 180)
                {
                    dragSpeed = lastPosition1 - lastPosition3;
                }
                else
                {
                    dragSpeed = (360 - (lastPosition1 - lastPosition3));
                }
            }

            if (lastPosition3 > lastPosition1)
            {
                if (lastPosition3 - lastPosition1 < 180)
                {
                    dragSpeed = lastPosition3 - lastPosition1 * -1;
                }
                else
                {
                    dragSpeed = 360 - (lastPosition3 - lastPosition1) * -1;
                }
            }

            if (dragSpeed > dragMaxSpeed)
            {
                dragSpeed = dragMaxSpeed;
            }
            if (dragSpeed < -dragMaxSpeed)
            {
                dragSpeed = -dragMaxSpeed;
            }

        }
    }
}
